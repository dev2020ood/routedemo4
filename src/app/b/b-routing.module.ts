import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { B1Component } from './b1/b1.component';
import { B2Component } from './b2/b2.component';

const routes: Routes = [{path:"1",component:B1Component},
{path:"2",component:B2Component} ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BRoutingModule { }
