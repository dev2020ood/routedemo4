import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavComponent } from './nav/nav.component';
import { NotImplementedComponent } from './not-implemented/not-implemented.component';

const routes: Routes = [
  {path:"A",component:NavComponent,loadChildren: () => import('../app/a/a.module').then(m => m.AModule)},
  {path:"B",component:NavComponent,loadChildren: () => import('../app/b/b.module').then(m => m.BModule)},
  {path:"C",component:NavComponent,loadChildren: () => import('../app/c/c.module').then(m => m.CModule)},
 /* {path:":directory", children:[
    {path:":num",component:NotImplementedComponent}
   
  ]},*/
  {path:"**",component:NotImplementedComponent}
    
  


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
