import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ARoutingModule } from './a-routing.module';
import { A1Component } from './a1/a1.component';
import { A2Component } from './a2/a2.component';


@NgModule({
  declarations: [A1Component, A2Component],
  imports: [
    CommonModule,
    ARoutingModule
  ]
})
export class AModule { }
