import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { A1Component } from './a1/a1.component';
import { A2Component } from './a2/a2.component';

const routes: Routes = [
  {path:"1",component:A1Component},
  {path:"2",component:A2Component},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ARoutingModule { }
